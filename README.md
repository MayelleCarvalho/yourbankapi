# __API Your Bank__

_Segue uma breve documentação para explicar o projeto teste do seletivo da Infoway: desenvolvimento de uma API para operações de CRUD de entidades do tipo instituição financeira (Bancos)._

## __Modelagem da API:__ diagramas de caso de uso e classes

### Diagrama de Classes: 
![](documentos/yourbank_diagrama_classe.png)

### Diagrama de Caso de uso
![](documentos/yourbank_diagrama_use_case.png)

## Endpoints da API:


- MÉTODO: GET ("localhost:8080/_nomedaentidadenoplural_") - Lista os OBJETOS;
- MÉTODO: GET ("localhost:8080/_nomedaentidadenoplural_/{id}") - Lista detalhe do OBJETO;
- MÉTODO: POST ("localhost:8080/_nomedaentidadenoplural_/novo") - Cria um novo OBJETO;
- MÉTODO: PUT ("localhost:8080/_nomedaentidadenoplural_/{id}/editar") - Edita o OBJETO;
- MÉTODO: DELETE ("localhost:8080/_nomedaentidadenoplural_/{id}/excluir") -Deleta o OBJETO;
- MÉTODO: POST ("localhost:8080/transacoes/_tipotransacao_/novo") - Cria um transacao do tipo depositos, transferencias ou saques;


### Imagem de exemplo de funcionamento da api com o postman:

![](documentos/teste_postman/get_objetos_postman.png )

## Requisição de Token:


### ENDPOINT Para obter o token: 
- Método GET - localhost:8080/oauth/token 
- Parâmetros padrao para primeiro acesso, depois do primeiro acesso pode sercriado um Usuário e passado como parâmetro o username e o password do usuario criado:
-- grant_type: password
-- username: admin
-- password: admin

- Authorization Basic:
-- username: client-id
-- password: secret-id

## Quando enviado os parâmetros e o Auth basic conforme a imagem abaixo será gerado o novo token com seu tempo de expiração.

### Imagem do teste de requisição do token no postman

![](documentos/teste_postman/get_token_postman.png)


## Após a geração do token é só passá-lo no Header como autenticação para as requisições da api com o tipo Bearer, conforme a imagem abaixo:


![](documentos/teste_postman/post_objeto_com_token_postman.png)


