package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Conta;
import br.com.yourbank.yourbankapi.repository.ContaRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ContaControllerTest {

    @InjectMocks
    ContaController controller;

    @Mock
    ContaRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testeGetTodasContas() {
        List<Conta> list = new ArrayList<Conta>();

        Conta conta1 = new Conta();
        conta1.setNumero(450);

        Conta conta2 = new Conta();
        conta2.setNumero(460);

        list.add(conta1);
        list.add(conta2);

        when(repository.findAll()).thenReturn(list);

        List<Conta> contas = controller.getTodos();

        assertEquals(2, contas.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testeGetContaPorId() {
        Conta conta = new Conta();
        conta.setId(1L);
        conta.setNumero(450);

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(conta));

        Conta ct = controller.getPorId(1L).get();

        assertEquals(conta.getNumero(), ct.getNumero());
    }

    @Test
    public void testePostConta() {
        Conta conta = new Conta();
        conta.setId(1L);
        conta.setNumero(450);

        when(repository.save(conta)).thenReturn(conta);

        ResponseEntity<Object> result = controller.salvar(conta);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());

        verify(repository, times(1)).save(conta);
    }

    @Test
    public void testePutConta(){
        Conta conta = new Conta();
        conta.setId(1L);
        conta.setNumero(450);

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(conta));

        Conta ct = controller.getPorId(1L).get();

        ct.setNumero(455);

        when(repository.save(ct)).thenReturn(ct);

        Conta ctEditado = controller.editarPorId(1l, ct);

        assertEquals(ct.getNumero(), ctEditado.getNumero());
    }

    @Test
    public void testeDeleteConta(){
        Conta conta = new Conta();
        conta.setId(1L);
        conta.setNumero(450);

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(conta));

        ResponseEntity<Object> result = controller.excluirPorId(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
