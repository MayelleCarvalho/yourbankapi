package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Usuario;
import br.com.yourbank.yourbankapi.repository.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioControllerTest {

    @InjectMocks
    UsuarioController controller;

    @Mock
    UsuarioRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testeGetTodosUsuarios() {
        List<Usuario> list = new ArrayList<Usuario>();

        Usuario usuario1 = new Usuario();
        usuario1.setUsername("teste1");

        Usuario usuario2 = new Usuario();
        usuario2.setUsername("teste2");

        list.add(usuario1);
        list.add(usuario2);

        when(repository.findAll()).thenReturn(list);

        List<Usuario> usuarios = controller.getTodos();

        assertEquals(2, usuarios.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testeGetUsuarioPorId() {
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setUsername("teste");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(usuario));

        Usuario us = controller.getPorId(1L).get();

        assertEquals("teste", us.getUsername());
    }

    @Test
    public void testePostUsuario() {
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setUsername("teste");

        when(repository.save(usuario)).thenReturn(usuario);

        ResponseEntity<Object> result = controller.salvar(usuario);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());

        verify(repository, times(1)).save(usuario);
    }

    @Test
    public void testePutUsuario(){
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setUsername("teste1");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(usuario));

        Usuario us = controller.getPorId(1L).get();

        us.setUsername("teste2");

        when(repository.save(us)).thenReturn(us);

        Usuario usuarioEditado = controller.editarPorId(1l, us);

        assertEquals("teste2", usuarioEditado.getUsername());
    }

    @Test
    public void testeDeleteUsuario(){
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setUsername("teste");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(usuario));

        ResponseEntity<Object> result = controller.excluirPorId(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
