package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Agencia;
import br.com.yourbank.yourbankapi.repository.AgenciaRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AgenciaControllerTest {

    @InjectMocks
    AgenciaController agenciaController;

    @Mock
    AgenciaRepository agenciaRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testeGetTodosAgencias() {
        List<Agencia> list = new ArrayList<Agencia>();

        Agencia agencia1 = new Agencia();
        agencia1.setDescricao("Teste agencia 1");

        Agencia agencia2 = new Agencia();
        agencia1.setDescricao("Teste agencia 2");

        list.add(agencia1);
        list.add(agencia2);

        when(agenciaRepository.findAll()).thenReturn(list);

        List<Agencia> agencias = agenciaController.getTodos();

        assertEquals(2, agencias.size());
        verify(agenciaRepository, times(1)).findAll();
    }

    @Test
    public void testeGetAgenciaPorId() {
        Agencia agencia = new Agencia();
        agencia.setId(1L);
        agencia.setDescricao("Teste agência");

        when(agenciaRepository.findById(1L)).thenReturn(java.util.Optional.of(agencia));

        Agencia agc = agenciaController.getPorId(1L).get();

        assertEquals("Teste agência", agc.getDescricao());
    }

    @Test
    public void testePostAgencia() {
        Agencia agencia = new Agencia();
        agencia.setId(1L);
        agencia.setDescricao("Teste agência");

        when(agenciaRepository.save(agencia)).thenReturn(agencia);

        ResponseEntity<Object> result = agenciaController.salvar(agencia);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());

        verify(agenciaRepository, times(1)).save(agencia);
    }

    @Test
    public void testePutAgencia(){
        Agencia agencia = new Agencia();
        agencia.setId(1L);
        agencia.setDescricao("Teste agência");

        when(agenciaRepository.findById(1L)).thenReturn(java.util.Optional.of(agencia));

        Agencia agc = agenciaController.getPorId(1L).get();

        agc.setDescricao("Teste Agência editado");

        when(agenciaRepository.save(agc)).thenReturn(agc);

        Agencia agcEditada = agenciaController.editarPorId(1l, agc);

        assertEquals("Teste Agência editado", agcEditada.getDescricao());
    }

    @Test
    public void testeDeleteAgencia(){
        Agencia agencia = new Agencia();
        agencia.setId(1L);
        agencia.setDescricao("Teste agência");

        when(agenciaRepository.findById(1L)).thenReturn(java.util.Optional.of(agencia));

        ResponseEntity<Object> result = agenciaController.excluirPorId(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

}
