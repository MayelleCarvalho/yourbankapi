package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Transacao;
import br.com.yourbank.yourbankapi.repository.TransacaoRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class TransacaoControllerTest {

    @InjectMocks
    TransacaoController controller;

    @Mock
    TransacaoRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testeGetTodosTransacoes() {
        List<Transacao> list = new ArrayList<Transacao>();

        Transacao transacao1 = new Transacao();
        transacao1.setValor(BigDecimal.valueOf(200.0));

        Transacao transacao2 = new Transacao();
        transacao2.setValor(BigDecimal.valueOf(200.0));

        list.add(transacao1);
        list.add(transacao2);

        when(repository.findAll()).thenReturn(list);

        List<Transacao> transacoes = controller.getTodos();

        assertEquals(2, transacoes.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testeGetTransacaoPorId() {
        Transacao transacao = new Transacao();
        transacao.setId(1L);
        transacao.setValor(BigDecimal.valueOf(200.0));

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(transacao));

        Transacao ts = controller.getPorId(1L).get();

        assertEquals(transacao.getValor(), ts.getValor());
    }

    @Test
    public void testePostTransacao() {
        Transacao transacao = new Transacao();
        transacao.setId(1L);
        transacao.setValor(BigDecimal.valueOf(250.0));

        when(repository.save(transacao)).thenReturn(transacao);

        ResponseEntity<Object> result = controller.salvar(transacao);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());

        verify(repository, times(1)).save(transacao);
    }

    @Test
    public void testePutTransacao(){
        Transacao transacao = new Transacao();
        transacao.setId(1L);
        transacao.setValor(BigDecimal.valueOf(200.0));

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(transacao));

        Transacao ts = controller.getPorId(1L).get();

        ts.setValor(BigDecimal.valueOf(250.0));

        when(repository.save(ts)).thenReturn(ts);

        Transacao tsEditado = controller.editarPorId(1l, ts);

        assertEquals(ts.getValor(), tsEditado.getValor());
    }

    @Test
    public void testeDeleteTransacao(){
        Transacao transacao = new Transacao();
        transacao.setId(1L);
        transacao.setValor(BigDecimal.valueOf(200.0));

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(transacao));

        ResponseEntity<Object> result = controller.excluirPorId(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
