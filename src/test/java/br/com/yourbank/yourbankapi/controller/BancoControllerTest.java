package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Banco;
import br.com.yourbank.yourbankapi.repository.BancoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BancoControllerTest {

    @InjectMocks
    BancoController controller;

    @Mock
    BancoRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testeGetTodosBancos() {
        List<Banco> list = new ArrayList<Banco>();

        Banco banco1 = new Banco();
        banco1.setRazaoSocial("Teste Banco 1");

        Banco banco2 = new Banco();
        banco2.setRazaoSocial("Teste Banco 2");

        list.add(banco1);
        list.add(banco2);

        when(repository.findAll()).thenReturn(list);

        List<Banco> bancos = controller.getTodos();

        assertEquals(2, bancos.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testeGetBancoPorId() {
        Banco banco = new Banco();
        banco.setId(1L);
        banco.setRazaoSocial("Teste Banco");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(banco));

        Banco bc = controller.getPorId(1L).get();

        assertEquals("Teste Banco", bc.getRazaoSocial());
    }

    @Test
    public void testePostBanco() {
        Banco banco = new Banco();
        banco.setId(1L);
        banco.setNomeFantasia("Teste Banco");

        when(repository.save(banco)).thenReturn(banco);

        ResponseEntity<Object> result = controller.salvar(banco);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());

        verify(repository, times(1)).save(banco);
    }

    @Test
    public void testePutBanco(){
        Banco banco = new Banco();
        banco.setId(1L);
        banco.setRazaoSocial("Teste Banco");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(banco));

        Banco bc = controller.getPorId(1L).get();

        bc.setRazaoSocial("Teste Banco editado");

        when(repository.save(bc)).thenReturn(bc);

        Banco bcEditado = controller.editarPorId(1l, bc);

        assertEquals("Teste Banco editado", bcEditado.getRazaoSocial());
    }

    @Test
    public void testeDeleteBanco(){
        Banco banco = new Banco();
        banco.setId(1L);
        banco.setRazaoSocial("Teste Banco");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(banco));

        ResponseEntity<Object> result = controller.excluirPorId(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

}
