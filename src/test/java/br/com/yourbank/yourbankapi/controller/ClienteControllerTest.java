package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Cliente;
import br.com.yourbank.yourbankapi.repository.ClienteRepository;
import br.com.yourbank.yourbankapi.repository.ClienteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {

    @InjectMocks
    ClienteController controller;

    @Mock
    ClienteRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testeGetTodosClientes() {
        List<Cliente> list = new ArrayList<Cliente>();

        Cliente cliente1 = new Cliente();
        cliente1.setNome("Teste Cliente 1");

        Cliente cliente2 = new Cliente();
        cliente2.setNome("Teste Cliente 2");

        list.add(cliente1);
        list.add(cliente2);

        when(repository.findAll()).thenReturn(list);

        List<Cliente> clientes = controller.getTodos();

        assertEquals(2, clientes.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testeGetClientePorId() {
        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("Teste Cliente");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(cliente));

        Cliente cl = controller.getPorId(1L).get();

        assertEquals("Teste Cliente", cl.getNome());
    }

    @Test
    public void testePostCliente() {
        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("Teste Cliente");

        when(repository.save(cliente)).thenReturn(cliente);

        ResponseEntity<Object> result = controller.salvar(cliente);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());

        verify(repository, times(1)).save(cliente);
    }

    @Test
    public void testePutCliente(){
        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("Teste Cliente");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(cliente));

        Cliente cl = controller.getPorId(1L).get();

        cl.setNome("Teste Cliente editado");

        when(repository.save(cl)).thenReturn(cl);

        Cliente clEditado = controller.editarPorId(1l, cl);

        assertEquals("Teste Cliente editado", clEditado.getNome());
    }

    @Test
    public void testeDeleteCliente(){
        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("Teste Cliente");

        when(repository.findById(1L)).thenReturn(java.util.Optional.of(cliente));

        ResponseEntity<Object> result = controller.excluirPorId(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
