package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Agencia;
import br.com.yourbank.yourbankapi.model.Cliente;
import br.com.yourbank.yourbankapi.repository.AgenciaRepository;
import br.com.yourbank.yourbankapi.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({"/clientes"})
public class ClienteController {


    @Autowired
    private ClienteRepository clienteRepository;

    ClienteController(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @GetMapping
    public List<Cliente> getTodos() {
        return clienteRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<Cliente> getPorId(@PathVariable Long id) {
        return clienteRepository.findById(id);
    }

    @PostMapping(value = "/novo")
    public ResponseEntity<Object> salvar(@RequestBody Cliente cliente){
        clienteRepository.save(cliente);
        return new ResponseEntity<>("Cliente criado com sucesso", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}/editar")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Cliente editarPorId(@PathVariable("id") Long id,
                                               @RequestBody Cliente clienteEditado) {
        Cliente clienteOld = clienteRepository.findById(id).get();
        clienteOld.setNome(clienteEditado.getNome());
        clienteOld.setCpf(clienteEditado.getCpf());
        clienteOld.setDataNacimento(clienteEditado.getDataNacimento());
        clienteRepository.save(clienteOld);

        return clienteOld;
    }

    @DeleteMapping(value = "/{id}/excluir")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") long id) {
        Cliente cliente  = clienteRepository.findById(id).get();
        if (cliente != null) {
            clienteRepository.delete(cliente);
        }
        return new ResponseEntity<>("Cliente deletado com sucesso", HttpStatus.OK);
    }
}
