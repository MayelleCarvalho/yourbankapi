package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Conta;
import br.com.yourbank.yourbankapi.repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({"/contas"})
public class ContaController {

    @Autowired
    private ContaRepository contaRepository;

    ContaController(ContaRepository contaRepository) {
        this.contaRepository = contaRepository;
    }

    @GetMapping
    public List<Conta> getTodos() {
        return contaRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<Conta> getPorId(@PathVariable Long id) {
        return contaRepository.findById(id);
    }

    @PostMapping(value = "/novo")
    public ResponseEntity<Object> salvar(@RequestBody Conta conta){
        contaRepository.save(conta);
        return new ResponseEntity<>("Conta criada com sucesso", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}/editar")
    public @ResponseBody Conta editarPorId(@PathVariable("id") Long id,
                                               @RequestBody Conta contaEditada) {
        Conta contaOld = contaRepository.findById(id).get();
        contaOld.setNumero(contaEditada.getNumero());
        contaOld.setDigito(contaEditada.getDigito());
        contaOld.setTipoConta(contaEditada.getTipoConta());
        contaOld.setAgencia(contaEditada.getAgencia());
        contaOld.setSaldo(contaEditada.getSaldo());
        contaRepository.save(contaOld);

        return contaOld;
    }

    @DeleteMapping(value = "/{id}/excluir")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") long id) {
        Conta conta  = contaRepository.findById(id).get();
        if (conta != null) {
            contaRepository.delete(conta);
        }
        return new ResponseEntity<>("Conta deletada com sucesso", HttpStatus.OK);
    }


}
