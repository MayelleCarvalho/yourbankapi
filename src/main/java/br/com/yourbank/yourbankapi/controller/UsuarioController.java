package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Usuario;
import br.com.yourbank.yourbankapi.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({"/usuarios"})
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    UsuarioController(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @GetMapping
    public List<Usuario> getTodos() {
        return usuarioRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<Usuario> getPorId(@PathVariable Long id) {
        return usuarioRepository.findById(id);
    }

    @PostMapping(value = "/novo")
    public ResponseEntity<Object> salvar(@RequestBody Usuario usuario){
        usuario.setPassword(new BCryptPasswordEncoder().encode(usuario.getPassword()));
        usuarioRepository.save(usuario);
        return new ResponseEntity<>("Usuario criado com sucesso", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}/editar")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Usuario editarPorId(@PathVariable("id") Long id,
                                             @RequestBody Usuario usuarioEditado) {
        Usuario usuarioOld = usuarioRepository.findById(id).get();
        usuarioOld.setUsername(usuarioEditado.getUsername());
        usuarioOld.setPassword(usuarioEditado.getPassword());
        usuarioRepository.save(usuarioOld);

        return usuarioOld;
    }

    @DeleteMapping(value = "/{id}/excluir")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") long id) {
        Usuario usuario  = usuarioRepository.findById(id).get();
        if (usuario != null) {
            usuarioRepository.delete(usuario);
        }
        return new ResponseEntity<>("Usuario deletado com sucesso", HttpStatus.OK);
    }
}
