package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.enums.TipoTransacao;
import br.com.yourbank.yourbankapi.model.Conta;
import br.com.yourbank.yourbankapi.model.Transacao;
import br.com.yourbank.yourbankapi.repository.ContaRepository;
import br.com.yourbank.yourbankapi.repository.TransacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({"/transacoes"})
public class TransacaoController {

    @Autowired
    private TransacaoRepository transacaoRepository;
    @Autowired
    private ContaRepository contaRepository;

    TransacaoController(TransacaoRepository transacaoRepository) {
        this.transacaoRepository = transacaoRepository;
    }

    @GetMapping
    public List<Transacao> getTodos() {
        return transacaoRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<Transacao> getPorId(@PathVariable Long id) {
        return transacaoRepository.findById(id);
    }

    @PostMapping(value = "/novo")
    public ResponseEntity<Object> salvar(@RequestBody Transacao transacao){
        transacaoRepository.save(transacao);
        return new ResponseEntity<>("Transacao criada com sucesso", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}/editar")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Transacao editarPorId(@PathVariable("id") Long id,
                                               @RequestBody Transacao transacaoEditada) {
        Transacao transacaoOld = transacaoRepository.findById(id).get();
        transacaoOld.setValor(transacaoEditada.getValor());
        transacaoOld.setData(transacaoEditada.getData());
        transacaoOld.setDestinatario(transacaoEditada.getDestinatario());
        transacaoOld.setRemetente(transacaoEditada.getRemetente());
        transacaoOld.setTipoTransacao(transacaoEditada.getTipoTransacao());
        transacaoRepository.save(transacaoOld);

        return transacaoOld;
    }

    @DeleteMapping(value = "/{id}/excluir")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") long id) {
        Transacao transacao  = transacaoRepository.findById(id).get();
        if (transacao != null) {
            transacaoRepository.delete(transacao);
        }
        return new ResponseEntity<>("Agencia deletada com sucesso", HttpStatus.OK);
    }

    @PostMapping(value = "depositos/novo")
    public ResponseEntity<Object> realizarDeposito(@RequestBody Transacao transacao){
        transacao.setTipoTransacao(TipoTransacao.DEPOSITO);
        transacao.setData(Calendar.getInstance());
        transacao.setRemetente(transacao.getRemetente());
        transacao.setDestinatario(transacao.getDestinatario());

        transacaoRepository.save(transacao);
        return new ResponseEntity<>("Depósito realizado com sucesso", HttpStatus.CREATED);
    }

    @PostMapping(value = "transferencias/novo")
    public ResponseEntity<Object> realizarTransferencia(@RequestBody Transacao transacao){
        transacao.setTipoTransacao(TipoTransacao.TRANSFERENCIA);
        transacao.setData(Calendar.getInstance());
        transacao.setRemetente(transacao.getRemetente());
        transacao.setDestinatario(transacao.getDestinatario());

        transacaoRepository.save(transacao);
        return new ResponseEntity<>("Transferência realizada com sucesso", HttpStatus.CREATED);
    }

    @PostMapping(value = "saques/novo")
    public ResponseEntity<Object> realizarSaque(@RequestBody Transacao transacao){
        transacao.setTipoTransacao(TipoTransacao.SAQUE);
        transacao.setData(Calendar.getInstance());
        transacao.setRemetente(transacao.getRemetente());
        transacao.setDestinatario(transacao.getDestinatario());

        transacaoRepository.save(transacao);
        return new ResponseEntity<>("Saque realizado com sucesso", HttpStatus.CREATED);
    }

    @GetMapping(value = "extrato/{id}")
    public List<Transacao> extratoPorContaId(@PathVariable Long id){
        Conta remetente = contaRepository.findById(id).get();
        return transacaoRepository.findTransacaoByRemetente(remetente);
    }
}
