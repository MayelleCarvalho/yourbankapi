package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Agencia;
import br.com.yourbank.yourbankapi.model.Banco;
import br.com.yourbank.yourbankapi.repository.AgenciaRepository;
import br.com.yourbank.yourbankapi.repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({"/bancos"})
public class BancoController {

    @Autowired
    private BancoRepository repository;

    BancoController(BancoRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public List<Banco> getTodos() {
        return repository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<Banco> getPorId(@PathVariable Long id) {
        return repository.findById(id);
    }

    @PostMapping(value = "/novo")
    public ResponseEntity<Object> salvar(@RequestBody Banco banco){
        repository.save(banco);
        return new ResponseEntity<>("Inclusão realizada com sucesso", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}/editar")
    public @ResponseBody Banco editarPorId(@PathVariable("id") Long id,
                                             @RequestBody Banco bancoEditado) {
        Banco bancoOld = repository.findById(id).get();
        bancoOld.setRazaoSocial(bancoEditado.getRazaoSocial());
        bancoOld.setNomeFantasia(bancoEditado.getNomeFantasia());
        bancoOld.setCodigo(bancoEditado.getCodigo());
        bancoOld.setCnpj(bancoEditado.getCnpj());
        repository.save(bancoOld);

        return bancoOld;
    }

    @DeleteMapping(value = "/{id}/excluir")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") long id) {
        Banco banco  = repository.findById(id).get();
        if (banco != null) {
            repository.delete(banco);
        }
        return new ResponseEntity<>("Exclusão realizada com sucesso", HttpStatus.OK);
    }

}
