package br.com.yourbank.yourbankapi.controller;

import br.com.yourbank.yourbankapi.model.Agencia;
import br.com.yourbank.yourbankapi.model.Banco;
import br.com.yourbank.yourbankapi.repository.AgenciaRepository;
import br.com.yourbank.yourbankapi.repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping({"/agencias"})
public class AgenciaController {

    @Autowired
    private AgenciaRepository repository;

    AgenciaController(AgenciaRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public List<Agencia> getTodos() {
        return repository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<Agencia> getPorId(@PathVariable Long id) {
        return repository.findById(id);
    }

    @PostMapping(value = "/novo")
    public ResponseEntity<Object> salvar(@RequestBody Agencia agencia){
        Banco banco = new Banco();
        banco.setId(1l);
        agencia.setBanco(banco);
        repository.save(agencia);
        return new ResponseEntity<>("Inclusão realizada com sucesso", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}/editar")
    public @ResponseBody Agencia editarPorId(@PathVariable("id") Long id,
                                           @RequestBody Agencia agenciaEditada) {
        Agencia agenciaOld = repository.findById(id).get();
        agenciaOld.setDescricao(agenciaEditada.getDescricao());
        agenciaOld.setDigito(agenciaEditada.getDigito());
        agenciaOld.setBanco(agenciaEditada.getBanco());
        agenciaOld.setCnpj(agenciaEditada.getCnpj());
        agenciaOld.setNumero(agenciaEditada.getNumero());
        repository.save(agenciaOld);

        return agenciaOld;
    }

    @DeleteMapping(value = "/{id}/excluir")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") long id) {
        Agencia agencia  = repository.findById(id).get();
        if (agencia != null) {
            repository.delete(agencia);
        }
        return new ResponseEntity<>("Exclusão realizada com sucesso", HttpStatus.OK);
    }

}
