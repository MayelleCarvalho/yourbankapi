package br.com.yourbank.yourbankapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Agencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String descricao;
    private String cnpj;

    private Integer numero;
    private Integer digito;

    @ManyToOne
    private Banco banco;

    @OneToMany(mappedBy = "agencia", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private Set<Conta> contas;
}
