package br.com.yourbank.yourbankapi.model;

import br.com.yourbank.yourbankapi.enums.TipoConta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Conta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer numero;
    private Integer digito;

    @ManyToOne
    private Cliente cliente;
    @ManyToOne
    private Agencia agencia;

    private TipoConta tipoConta;

    private BigDecimal saldo;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy ="remetente", targetEntity = Transacao.class , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Transacao> transacoesRealizadas;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy ="destinatario", targetEntity = Transacao.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Transacao> transacoesRecebidas;
}
