package br.com.yourbank.yourbankapi.model;

import br.com.yourbank.yourbankapi.enums.TipoTransacao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Transacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Calendar data;

    private BigDecimal valor;

    @ManyToOne
    @JoinColumn(name = "remetente_id")
    private Conta remetente;
    @ManyToOne
    @JoinColumn(name = "destinatario_id")
    private Conta destinatario;

    private TipoTransacao tipoTransacao;
}
