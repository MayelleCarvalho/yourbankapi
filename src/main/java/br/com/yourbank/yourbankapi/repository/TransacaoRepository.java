package br.com.yourbank.yourbankapi.repository;

import br.com.yourbank.yourbankapi.model.Conta;
import br.com.yourbank.yourbankapi.model.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransacaoRepository extends JpaRepository <Transacao, Long> {

    List<Transacao> findTransacaoByRemetente(Conta remetente);
}
