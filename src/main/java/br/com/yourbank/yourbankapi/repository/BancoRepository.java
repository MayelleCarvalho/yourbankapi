package br.com.yourbank.yourbankapi.repository;

import br.com.yourbank.yourbankapi.model.Banco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BancoRepository extends JpaRepository <Banco, Long> {
}
