package br.com.yourbank.yourbankapi.repository;

import br.com.yourbank.yourbankapi.model.Conta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContaRepository extends JpaRepository <Conta, Long> {
}
