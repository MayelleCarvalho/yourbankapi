package br.com.yourbank.yourbankapi.repository;

import br.com.yourbank.yourbankapi.model.Agencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgenciaRepository extends JpaRepository<Agencia, Long> {
}
