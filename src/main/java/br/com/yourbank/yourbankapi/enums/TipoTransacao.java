package br.com.yourbank.yourbankapi.enums;

public enum TipoTransacao {

    TRANSFERENCIA (1,"Transferência"),
    DEPOSITO (2,"Depósito"),
    SAQUE(3,"Saque");

    private int id;
    private String descricao;

    TipoTransacao(int id, String descricao) {
        this.id =id;
        this.descricao=descricao;
    }

    public int getId(){ return id; }

    public String getDescricao(){
        return descricao;
    }
}
