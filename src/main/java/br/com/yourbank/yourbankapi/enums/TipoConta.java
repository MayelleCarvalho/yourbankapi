package br.com.yourbank.yourbankapi.enums;

public enum TipoConta {

    CORRENTE (1,"Corrente"),
    POUPANCA(2, "Poupança"),;

    private int id;
    private String descricao;

    TipoConta(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public int getId(){ return id;  }

    public String getDescricao(){
        return descricao;
    }
}
