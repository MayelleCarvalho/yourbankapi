package br.com.yourbank.yourbankapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YourbankapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(YourbankapiApplication.class, args);
	}

}
